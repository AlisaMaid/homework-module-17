#include <iostream>

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double newX, double newY, double newZ) : x(newX),y(newY),z(newZ)
    {}

    void PrintXYZ()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }
    double GetLength()
    {
        return (sqrt(x*x+y*y+z*z));
    }
};

int main()
{
    Vector test(3, 4, 5);
    test.PrintXYZ();
    std::cout << "Length = " << test.GetLength();
}
